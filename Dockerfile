FROM nginx:latest
COPY dist/mvp_ui /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf
COPY fullchain.pem /etc/ssl/
COPY privkey.pem /etc/ssl/
EXPOSE 80
