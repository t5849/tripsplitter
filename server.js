const express = require('express');
const path = require('path');
const app = express();
app.use(express.static(__dirname + '/dist/mvp_ui'));
app.get('/*', function(req,res) {
res.sendFile(path.join(__dirname + '/dist/mvp_ui/index.html'));});
app.listen(process.env.PORT || 80);
