import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Subject } from '@app/model/subject'

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  private remoteUrl: string

  constructor(private http: HttpClient) {
	this.remoteUrl = 'https://traveling-together.hopto.org:8183/maps/subjects/'
	//this.remoteUrl = 'http://51.250.102.252:8083/maps/subjects/'
    //this.remoteUrl = 'https://mighty-dusk-86087.herokuapp.com/maps/subjects/'
  }
  
  public getPartSubjects(offset: number = 0, amount: number = 6, filter: string = '')
  {
	return this.http.get<Subject[]>(this.remoteUrl + `?start=${offset}&count=${amount}&search=${filter}`)
  }
}
