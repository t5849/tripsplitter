import { Injectable } from '@angular/core'
import { Trip } from '@app/model/trip'
import { Invite } from '@app/model/invite'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'

import { CookieService  } from 'ngx-cookie-service'

@Injectable({
  providedIn: 'root'
})
export class TripService {
  private remoteUrl: string
  private remoteUrlInvites: string
  
  constructor(private http: HttpClient, private cookieService: CookieService) {
	this.remoteUrl = 'https://traveling-together.hopto.org:8183/trips/'
	this.remoteUrlInvites = 'https://traveling-together.hopto.org:8183/invite/'
    //this.remoteUrl = 'http://mighty-dusk-86087.herokuapp.com/trips/'
    //this.remoteUrlInvites = 'http://mighty-dusk-86087.herokuapp.com/invite/'
	//this.remoteUrl = 'http://164.92.198.59:8083/trips/'
	//this.remoteUrl = 'http://51.250.102.252:8083/trips/'
  }
  
  public getWeather(lat: number, lon: number) {
    //const params = new HttpParams().append('lat', 51.05).append('lon', 40.15)
    //const params = new HttpParams().append('lat', 40.15).append('lon', 51.05)
	const params = new HttpParams().append('lat', lat).append('lon', lon)
	return this.http.get<any>('https://traveling-together.hopto.org:8183/weather/', {params})
  }
  
  public getHeaders(): HttpHeaders {
    const headers = new HttpHeaders({
      'Content-Type':  'application/json',
	  'Authorization': `${this.cookieService.get('access')}`
    });
	return headers;
  }
  
  public create(trip: Trip)
  {
    const options = { headers: this.getHeaders() };
	console.log(options)
	return this.http.post<Trip>(this.remoteUrl + 'test', trip, options)
  }
  
  public getTripById(tripId: number)
  {
	const headers = new HttpHeaders()
    const params = new HttpParams()
	return this.http.get<Trip>(this.remoteUrl + tripId, {headers, params})
  }
  
  public getTrips()
  {
	return this.http.get<Trip[]>(this.remoteUrl);
  }
  
  public update(trip: Trip)
  {
	return this.http.put<Trip>(this.remoteUrl, trip)
  }
  
  public deleteEvent(trip: Trip, eventId: number)
  {
	return this.http.request('DELETE', this.remoteUrl + 'event/' + eventId, { body: trip })
  }
  
  public getInviteByTripId(tripId: number)
  {
	const headers = new HttpHeaders()
	let params = new HttpParams().append('tripId', tripId)
	return this.http.get<Invite>(this.remoteUrlInvites, {headers, params})
  }
  
  public getInvitesByTripIds(tripId: number[])
  {
	const headers = new HttpHeaders()
	let params = new HttpParams()
	tripId.forEach((x: number) => {
		params = params.append('tripId', x)
	})
	return this.http.get<Invite[]>(this.remoteUrlInvites + 'getInvitesByTripIds', {headers, params})
  }
  
  public revoke(tripId: number)
  {
	const headers = new HttpHeaders()
	let params = new HttpParams().append('tripId', tripId)
	return this.http.get<Invite>(this.remoteUrlInvites + 'revoke', {headers, params})
  }
  
  public getTripByInvite(inviteId: string)
  {
	const headers = new HttpHeaders()
    const params = new HttpParams().append('inviteId', inviteId)
	return this.http.get<Trip>(this.remoteUrl + 'join', {headers, params})
  }
  
  public delete(trip: Trip)
  {
	return this.http.delete(this.remoteUrl + trip.tripId);
  }
  
  public test()
  {
    const headers = new HttpHeaders()
    const params = new HttpParams()
  }
}
