import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { CookieService  } from 'ngx-cookie-service'

import { Goal } from '@app/model/goal'

@Injectable({
  providedIn: 'root'
})
export class GoalService {
  private url: string
  private payUrl: string
  
  constructor(private http: HttpClient, private cookieService: CookieService) {
	  this.url = 'https://traveling-together.hopto.org:8181/goals/'
	  this.payUrl = 'https://traveling-together.hopto.org:8181/pay/'
	//this.url = 'http://51.250.102.252:8081/goals/'
  }
  
  public getHeaders(): HttpHeaders {
    const headers = new HttpHeaders({
      'Content-Type':  'application/json',
	  'Authorization': `${this.cookieService.get('access')}`
    });
	return headers;
  }
  
  public doPayment(receiver: string, tripId: number, senderId: number, recipientId: number, sum: number) {
	const requestOptions: Object = {
	  headers: new HttpHeaders().append('Authorization', `Bearer ${this.cookieService.get('access')}`),
	  responseType: 'text'
	}
	
	const py = {
		"receiver": receiver,
		"quickPayForm": "donate",
		"targets": "payment",
		"paymentType": "AC",
		"sum": sum,
		"tripId": tripId,
		"senderId": senderId,
		"recipientId": recipientId,
	};
	//console.log(py)
	return this.http.post<string>(this.payUrl, py, requestOptions)
  }
  
  public success(paymentId: string)
  {
	const options = { headers: this.getHeaders() }
	console.log(options)
	return this.http.post<any>(this.payUrl + 'success?paymentId=' + paymentId, options) //POST
  }
  
  public create(goal: Goal)
  {
    const options = { headers: this.getHeaders() };
	console.log(options)
	return this.http.post<Goal>(this.url, goal, options)
  }
  
  public update(goal: Goal)
  {
    const options = { headers: this.getHeaders() }
	return this.http.put<Goal>(this.url, goal, options)
  }
  
  public getGoalById(goalId: number)
  {
	const options = { headers: this.getHeaders() }
	return this.http.get<Goal>(this.url + goalId, options)
  }
}
