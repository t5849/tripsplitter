import { Injectable } from '@angular/core'

import { PoiType } from '@app/model/poi-type'

import Cafes from './Cafes.json';
import Cinemas from './Cinemas.json';
import Hotels from './Hotels.json';
import Monuments from './Monuments.json';
import Institutions from './Institutions.json';
import Museums from './Museums.json';
import Bars from './Bars.json';
import Squares from './Squares.json';
import Malls from './Malls.json';
import Zoos from './Zoos.json';
import Stadiums from './Stadiums.json';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class PoiService {
  private url: string
  
  constructor(private http: HttpClient) {
	  this.url = 'https://traveling-together.hopto.org:8183/maps/'
	//this.url = 'http://51.250.102.252:8083/maps/'
  }
  
  getPoiTypes() {
	  return [
		new PoiType('Сafes and restaurants', 'red'),
		new PoiType('Theaters and cinema', 'green'),
		new PoiType('Hotels', 'cyan'),
		new PoiType('Sights', 'magenta'),
		new PoiType('Institutes', 'orange'),
		new PoiType('Museums', 'pink'),
		new PoiType('Bars', 'blue'),
		new PoiType('Parks and Squares', 'brown'),
		new PoiType('Malls', 'black'),
		new PoiType('Zoos', 'gray'),
		new PoiType('Stadiums', 'yellow')
	  ]
  }
  
  getPoiTypes2() {
	  return this.http.get<PoiType[]>(this.url + 'types')
  }
  
  getPlacesVoronezh(poi: PoiType): any
  {
	  const places: any = {
		'Сafes and restaurants': Cafes,
		'Theaters and cinema': Cinemas,
		'Hotels': Hotels,
		'Sights': Monuments,
		'Institutes': Institutions,
		'Museums': Museums,
		'Bars': Bars,
		'Parks and Squares': Squares,
		'Malls': Malls,
		'Zoos': Zoos,
		'Stadiums': Stadiums
	  }
	  return places[poi.typeName]
  }
  
  getPlacesOther(poi: PoiType, latitude: number, longitude: number)
  {
	return this.fetchPlacesFromYandex(poi.typeName, latitude, longitude)
  }
  
  fetchPlacesFromYandex(searchRequest: string, latitude: number, longitude: number)
  {
	return this.http.get<any>(`https://search-maps.yandex.ru/v1/?apikey=d61c2b2b-0556-403c-8f45-09f99c2775fd&text=${searchRequest}&lang=en_US&ll=${longitude},${latitude}&spn=1,1&rspn=1&results=500`)
  }
  
  getPlaces2(poi: PoiType): any
  {
	  const params = new HttpParams().append('type', poi.typeName)
	  return this.http.get<any[]>(this.url + 'objects', {params})
  }
}
