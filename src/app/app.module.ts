import {APP_INITIALIZER, ModuleWithProviders, NgModule} from '@angular/core'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {BrowserModule} from '@angular/platform-browser'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'

import { HttpClientModule, HttpClientJsonpModule  } from '@angular/common/http'
import { CookieService  } from 'ngx-cookie-service'

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxSpinnerModule } from 'ngx-spinner';


import {
    TUI_SANITIZER,
    TuiActiveZoneModule,
    TuiAutoFocusModule,
    TuiElementModule,
    TuiFilterPipeModule,
    TuiLetModule,
    TuiMapperPipeModule,
    TuiMediaModule,
} from '@taiga-ui/cdk'
import {
    TuiButtonModule,
    TuiCalendarModule,
    TuiDataListModule,
    TuiDialogModule,
    TuiDropdownControllerModule,
    TuiDropdownModule,
    TuiErrorModule,
    TuiExpandModule,
    TuiFormatNumberPipeModule,
    TuiFormatPhonePipeModule,
    TuiGroupModule,
    TuiHintControllerModule,
    TuiHintModule,
    TuiHostedDropdownModule,
    TuiLabelModule,
    TuiLinkModule,
    TuiLoaderModule,
    TuiManualHintModule,
    TuiModeModule,
    TuiNotificationModule,
    TuiNotificationsModule,
    TuiPointerHintModule,
    TuiPrimitiveCheckboxModule,
    TuiPrimitiveTextfieldModule,
    TuiRootModule,
    TuiScrollbarModule,
    TuiSvgModule,
    TuiSvgService,
    TuiTextfieldControllerModule,
    TuiTooltipModule,
	TuiThemeNightModule,
} from '@taiga-ui/core'
import {
    TuiAccordionModule,
    TuiActionModule,
    TuiAvatarModule,
    TuiBadgedContentModule,
    TuiBadgeModule,
    TuiBreadcrumbsModule,
    TuiCalendarMonthModule,
    TuiCalendarRangeModule,
    TuiCheckboxBlockModule,
    TuiCheckboxLabeledModule,
    TuiCheckboxModule,
    TuiComboBoxModule,
    TuiDataListWrapperModule,
    TuiDropdownContextModule,
    TuiDropdownSelectionModule,
    TuiFieldErrorModule,
    TuiFilterModule,
    TuiHighlightModule,
    TuiInputCopyModule,
    TuiInputCountModule,
    TuiInputDateModule,
    TuiInputDateRangeModule,
    TuiInputDateTimeModule,
    TuiInputFileModule,
    TuiInputInlineModule,
    TuiInputModule,
    TuiInputMonthModule,
    TuiInputMonthRangeModule,
    TuiInputNumberModule,
    TuiInputPasswordModule,
    TuiInputPhoneInternationalModule,
    TuiInputPhoneModule,
    TuiInputRangeModule,
    TuiInputSliderModule,
    TuiInputTagModule,
    TuiInputTimeModule,
    TuiIslandModule,
    TuiLazyLoadingModule,
    TuiLineClampModule,
    TuiMarkerIconModule,
    TuiMultiSelectModule,
    TuiPaginationModule,
    TuiPresentModule,
    TuiProgressModule,
    TuiProjectClassModule,
    TuiFilterByInputPipeModule,
    TuiRadioBlockModule,
    TuiRadioLabeledModule,
    TuiRadioListModule,
    TuiRadioModule,
    TuiSelectModule,
    TuiSliderModule,
    TuiStepperModule,
    TuiTabsModule,
    TuiTagModule,
    TuiTextAreaModule,
    TuiToggleModule,
} from '@taiga-ui/kit'

import * as icons from '@taiga-ui/icons'

import {
    TuiAxesModule,
    TuiBarModule,
    TuiBarChartModule,
    TuiLineChartModule,
    TuiLineDaysChartModule,
    TuiPieChartModule,
    TuiRingChartModule,
} from '@taiga-ui/addon-charts'
import {
    TuiCardModule,
    TuiInputCardModule,
    TuiInputCVCModule,
    TuiCurrencyPipeModule,
    TuiInputExpireModule,
    TuiMoneyModule,
} from '@taiga-ui/addon-commerce'
import {TuiColorPickerModule, TuiEditorModule, TuiEditorSocketModule} from '@taiga-ui/addon-editor'
import {
    TuiElasticStickyModule,
    TuiMobileCalendarModule,
    TuiMobileDialogModule,
    TuiPullToRefreshModule,
    TuiRippleModule,
    TuiSidebarModule,
    TuiTouchableModule,
} from '@taiga-ui/addon-mobile'
import {
    TuiReorderModule,
    TuiResizableColumnModule,
    TuiTablePaginationModule,
    TuiTableModule,
} from '@taiga-ui/addon-table'
import {NgDompurifySanitizer} from '@tinkoff/ng-dompurify'
import {PolymorpheusModule} from '@tinkoff/ng-polymorpheus'
import {AppComponent} from './app.component'
import {RouterModule} from '@angular/router'

import { RegisterComponent } from './component/register/register.component'
import { LoginComponent } from './component/login/login.component'
import { HomeComponent } from './component/home/home.component'
import { TripCreateComponent } from './component/trip-create/trip-create.component'
import { TripViewComponent } from './component/trip-view/trip-view.component'
import { TripJoinComponent } from './component/trip-join/trip-join.component'
import { UserInfoComponent } from './component/user-info/user-info.component'
import { Home2Component } from './component/home2/home2.component';
import { SuccessComponent } from './component/success/success.component';

import { Routes } from '@angular/router'
const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home-old', component: HomeComponent },
  { path: 'home', component: Home2Component },
  { path: 'trip-create', component: TripCreateComponent },
  { path: 'trip-view', component: TripViewComponent },
  { path: 'trip-join/:tripId', component: TripJoinComponent },
  { path: 'user-info', component: UserInfoComponent },
  { path: 'success', component: SuccessComponent }
]

import { AngularYandexMapsModule, YaConfig } from 'angular8-yandex-maps'

const mapConfig: YaConfig = {
  apikey: '484a978e-3024-4ca8-be48-444129567c52',
  lang: 'en_US',
}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { EditUsernameComponent } from './component/user-info/edit-username/edit-username.component';
import { ChangePasswordComponent } from './component/user-info/change-password/change-password.component';
import { AddPoiComponent } from './component/trip-view/add-poi/add-poi.component';
import { AddGoalComponent } from './component/trip-view/add-goal/add-goal.component';
import { EditQiwinameComponent } from './component/user-info/edit-qiwiname/edit-qiwiname.component';

import { DragDropModule} from '@angular/cdk/drag-drop';


import {TuiInputFilesModule} from '@taiga-ui/kit';

import * as KIT from '@taiga-ui/kit';

@NgModule({
    imports: [
		TuiInputFilesModule,
		NgxSpinnerModule,
		InfiniteScrollModule,
		DragDropModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        // Modules for main app.module that should be add once
        TuiRootModule,
        TuiDialogModule,
        TuiNotificationsModule,
        // Modules for your app modules where you use our components
        TuiAccordionModule,
        TuiActionModule,
        TuiActiveZoneModule,
        TuiAvatarModule,
        TuiBadgeModule,
        TuiBadgedContentModule,
        TuiButtonModule,
        TuiCalendarModule,
        TuiCalendarRangeModule,
        TuiCalendarMonthModule,
        TuiCardModule,
        TuiAxesModule,
        TuiLineChartModule,
        TuiLineDaysChartModule,
        TuiPieChartModule,
        TuiBarChartModule,
        TuiRingChartModule,
        TuiCheckboxModule,
        TuiCheckboxBlockModule,
        TuiCheckboxLabeledModule,
        TuiPrimitiveCheckboxModule,
        TuiColorPickerModule,
        TuiDataListModule,
        TuiDataListWrapperModule,
        TuiDropdownContextModule,
        TuiHostedDropdownModule,
        TuiErrorModule,
        TuiEditorModule,
        TuiEditorSocketModule,
        TuiExpandModule,
        TuiFieldErrorModule,
        TuiFilterModule,
        TuiGroupModule,
        TuiMarkerIconModule,
        TuiInputInlineModule,
        TuiInputModule,
        TuiInputDateModule,
        TuiInputCardModule,
        TuiInputCVCModule,
        TuiInputExpireModule,
        TuiInputCopyModule,
        TuiInputCountModule,
        TuiInputDateTimeModule,
        TuiInputFileModule,
        TuiInputMonthModule,
        TuiInputMonthRangeModule,
        TuiInputNumberModule,
        TuiInputPasswordModule,
        TuiInputPhoneModule,
        TuiInputRangeModule,
        TuiInputDateRangeModule,
        TuiInputSliderModule,
        TuiInputTagModule,
        TuiInputTimeModule,
        TuiInputPhoneInternationalModule,
        TuiPrimitiveTextfieldModule,
        TuiTextAreaModule,
        TuiIslandModule,
        TuiLabelModule,
        TuiLineClampModule,
        TuiLinkModule,
        TuiLoaderModule,
        TuiMoneyModule,
        TuiNotificationModule,
        TuiMobileDialogModule,
        TuiMobileCalendarModule,
        TuiPullToRefreshModule,
        TuiRadioModule,
        TuiRadioBlockModule,
        TuiRadioLabeledModule,
        TuiRadioListModule,
        TuiComboBoxModule,
        TuiMultiSelectModule,
        TuiSelectModule,
        TuiScrollbarModule,
        TuiInputRangeModule,
        TuiInputSliderModule,
        TuiSliderModule,
        TuiSvgModule,
        TuiReorderModule,
        TuiResizableColumnModule,
        TuiTablePaginationModule,
        TuiTagModule,
        TuiToggleModule,
        TuiTooltipModule,
        TuiBreadcrumbsModule,
        TuiPaginationModule,
        TuiStepperModule,
        TuiTabsModule,
        TuiAutoFocusModule,
        TuiDropdownModule,
        TuiDropdownSelectionModule,
        TuiElasticStickyModule,
        TuiElementModule,
        TuiHighlightModule,
        TuiHintModule,
        TuiLazyLoadingModule,
        TuiManualHintModule,
        TuiPointerHintModule,
        TuiLetModule,
        TuiMediaModule,
        TuiModeModule,
        TuiPresentModule,
        TuiProgressModule,
        TuiRippleModule,
        TuiSidebarModule,
        TuiDropdownControllerModule,
        TuiTouchableModule,
        TuiHintControllerModule,
        TuiTextfieldControllerModule,
        TuiMoneyModule,
        PolymorpheusModule,
        TuiFilterPipeModule,
        TuiFormatNumberPipeModule,
        TuiFormatPhonePipeModule,
        TuiMapperPipeModule,
        TuiTableModule,
        TuiBarModule,
        TuiCurrencyPipeModule,
        TuiProjectClassModule,
        TuiFilterByInputPipeModule,
		TuiThemeNightModule,
        RouterModule.forRoot(routes, { useHash: true }),
		HttpClientModule,
		HttpClientJsonpModule,
		AngularYandexMapsModule.forRoot(mapConfig),
    ],
	exports: [RouterModule],
    declarations: [AppComponent, RegisterComponent, LoginComponent, HomeComponent, TripCreateComponent, TripViewComponent, TripJoinComponent, UserInfoComponent, EditUsernameComponent, ChangePasswordComponent, AddPoiComponent, EditQiwinameComponent, AddGoalComponent, Home2Component, SuccessComponent],
    bootstrap: [AppComponent],
    //schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    providers: [
        // A workaround because StackBlitz does not support assets
        {
            provide: APP_INITIALIZER,
            multi: true,
            deps: [TuiSvgService],
            useFactory: iconsWorkaround,
        },
        /**
         * If you use unsafe icons or have kind of WYSISYG editor in your app
         *
         * Take a look at: https://github.com/TinkoffCreditSystems/ng-dompurify
         *
         * This library implements DOMPurify as Angular Sanitizer or Pipe.
         * It delegates sanitizing to DOMPurify and supports the same configuration.
         */
        {
            provide: TUI_SANITIZER,
            useClass: NgDompurifySanitizer,
        },
		CookieService
    ],
})
export class AppModule {}

// A workaround because StackBlitz does not support assets
export function iconsWorkaround(service: TuiSvgService): Function {
    return () => service.define({...icons, tuiCoreIcons: '', tuiKitIcons: ''})
}
