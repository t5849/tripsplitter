import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { GoalService } from '@app/service/goal.service'
import { Trip } from '@app/model/trip'
import { CookieService  } from 'ngx-cookie-service'

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {
  constructor(private route: ActivatedRoute, 
            private router: Router,
		  private goalService: GoalService, 
		private cookieService: CookieService) { 
		
			let paymentId = this.route.snapshot.queryParamMap.get('paymentId');
			console.log(paymentId)
			this.goalService.success(paymentId!).subscribe((result: any) => {
				console.log(result)
				this.router.navigate(['/trip-view'])
			}, error => {
				console.log(error)
				//location.replace('/#/home')
			})
		
  }
  
  ngOnInit(): void {
  }
}
