import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { TripService } from '@app/service/trip.service'
import { Trip } from '@app/model/trip'
import { CookieService  } from 'ngx-cookie-service'


@Component({
  selector: 'app-trip-join',
  templateUrl: './trip-join.component.html',
  styleUrls: ['./trip-join.component.less']
})
export class TripJoinComponent implements OnInit {
  constructor(private route: ActivatedRoute, 
            private router: Router,
		  private tripService: TripService, 
		private cookieService: CookieService) { 
	if (!cookieService.check('id'))
	{
		this.router.navigate(['/login'])
	}
	else
	{
		let userId = Number(cookieService.get('id'))
		this.route.params.subscribe(params => {
			let tripId = params['tripId']
			console.log(tripId)
			this.tripService.getTripByInvite(tripId).subscribe(result => this.join(result, userId), error => {
				console.log(error)
				location.replace('/#/home')
			})
		})
	}
  }
  
  join(trip: Trip, userId: number)
  {
	this.cookieService.set('requreReload', 'true')
	if (userId != trip.ownerId && trip.participantsId.filter(x => x === userId).length < 1)
	{
		trip.participantsId.push(userId)
		this.tripService.update(trip).subscribe(result => {
			this.router.navigate(['/trip-view'], { state: trip })
			console.log(result)
		})
	} else {
		console.log(trip)
		this.router.navigate(['/trip-view'], { state: trip })
	}
  }
  
  ngOnInit(): void {
  }
}
