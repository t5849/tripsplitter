import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TripJoinComponent } from './trip-join.component';

describe('TripJoinComponent', () => {
  let component: TripJoinComponent;
  let fixture: ComponentFixture<TripJoinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TripJoinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TripJoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
