import { Component, OnInit } from '@angular/core'
import { SubjectService } from '@app/service/subject.service'
import { Subject } from '@app/model/subject'
import {ChangeDetectorRef} from '@angular/core'
import {FormControl, FormGroup} from '@angular/forms'
import { NgxSpinnerService } from 'ngx-spinner';


import { CookieService  } from 'ngx-cookie-service'

@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.less'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class Home2Component implements OnInit {
  public subjects: Subject[]
  public filteredSubjects: Subject[]
  public subjectFilter: string
  public subjectFilterOld: string

  constructor(
	    private cs: CookieService, private cd : ChangeDetectorRef, private subjectService: SubjectService, private spinner: NgxSpinnerService) {
	if (this.cs.check('requreReload'))
		{
			this.cs.delete('requreReload')
			location.reload()
		}
	this.subjects = []
	this.filteredSubjects = []
	this.subjectFilter = ''
	this.subjectFilterOld = ''
	this.subjectService.getPartSubjects().subscribe(result => {
		this.subjects = result
		console.log(this.subjects)
	})
  }

  ngOnInit(): void {
  }
  
  public isDesktop = window.innerWidth > window.innerHeight
  onResize(e: any)
  {
	this.isDesktop = window.innerWidth > window.innerHeight
	this.cd.detectChanges()
  }
  
  getFilteredSubjects()
  {
	if (this.subjectFilterOld != this.subjectFilter)
	{
		this.subjectService.getPartSubjects(0, 30, this.subjectFilter).subscribe( result => {
		  this.filteredSubjects = result
		  this.cd.detectChanges()
		})
		this.subjectFilterOld = this.subjectFilter
	}
	if (this.filteredSubjects.length > 0 && this.subjectFilter != '')
	{
		return this.filteredSubjects
	}
	else
	{
		return this.subjects.filter(x => x.name.indexOf(this.subjectFilter) > -1)
	}
  }
  
  ready = true
  hasPosts = true
  onScroll() {
	console.log('next?')
    if (this.ready && this.hasPosts) {
      this.spinner.show();
      this.ready = false;
      this.next();
    }
  }
  
  sleep (time: number) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }
  
  next() {
	console.log('next!')
	this.subjectService.getPartSubjects(this.subjects.length).subscribe( result => {
	  if (result.length === 0 ) {
	    this.hasPosts = false;
	  }
	  
	  this.sleep(0).then(() => {
	    this.spinner.hide();
	    this.subjects = this.subjects.concat(result);
	    this.cd.detectChanges();
	    this.ready = true;
	  });
	});
  }
  
  readonly form = new FormGroup({
    subjectControl: new FormControl('')
  })
}
