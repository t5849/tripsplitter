import { Component, OnInit } from '@angular/core'
import {FormControl, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors} from '@angular/forms'

import { ActivatedRoute, Router } from '@angular/router'
import { Trip } from '@app/model/trip'
import { TripService } from '@app/service/trip.service'

import { CookieService  } from 'ngx-cookie-service'

import { ChangeDetectorRef } from '@angular/core'

@Component({
  selector: 'app-trip-create',
  templateUrl: './trip-create.component.html',
  styleUrls: ['./trip-create.component.less']
})
export class TripCreateComponent implements OnInit {
  trip: Trip
  error: string | null

  constructor(private router: Router, private tripService: TripService, private cookieService: CookieService, private cd: ChangeDetectorRef, private route: ActivatedRoute) {
    this.trip = new Trip()
	this.trip.latitude = Number(this.route.snapshot.paramMap.get('lat'))
	this.trip.longitude = Number(this.route.snapshot.paramMap.get('lon'))
	console.log(this.trip)
	this.error = null
  }

  ngOnInit(): void {
  }
  
  onSubmit()
  {
	this.setError(null)
	this.trip.ownerId = Number(this.cookieService.get('id'))
    this.tripService.create(this.trip).subscribe(result => this.onCreate(result), error => this.setError('Something went wrong...'))
  }

  onCreate(result: any) {
	console.log(result)
	if (result == null)
	{
		this.setError('Something went wrong...')
	}
	else
	{
		this.cookieService.set('requreReload', 'true')
		this.router.navigate(['/trip-view'], { state: result })
	}
  }
  
  setError(error: string | null) {
	this.error = error
	this.cd.detectChanges()
  }
  
  checkTripName: ValidatorFn = (field: AbstractControl):  ValidationErrors | null => { 
    let name = field.value
	if (name?.length < 3 || name?.length > 14) {
		return {
			lengthError: 'Trip name length should be between 3 and 14'
		}
	}
	return null
  }
  
  readonly tripForm = new FormGroup({
    tripNameControl: new FormControl('', [
	    Validators.required,
		this.checkTripName
	]),
  })
}
