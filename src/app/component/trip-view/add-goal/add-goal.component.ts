import { Component, OnInit, Inject } from '@angular/core'
import {FormControl, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors} from '@angular/forms'

import { Router } from '@angular/router'

import { Trip } from '@app/model/trip'
import { User } from '@app/model/user'
import { ParticipantInfo } from '@app/model/participant-info'
import { Event } from '@app/model/event'
import { Goal } from '@app/model/goal'

import { TripService } from '@app/service/trip.service'
import { GoalService } from '@app/service/goal.service'

import { CookieService  } from 'ngx-cookie-service'

import { ChangeDetectorRef } from '@angular/core'

import {TuiDialogContext} from '@taiga-ui/core'
import {POLYMORPHEUS_CONTEXT} from '@tinkoff/ng-polymorpheus';

@Component({
  selector: 'app-add-goal',
  templateUrl: './add-goal.component.html',
  styleUrls: ['./add-goal.component.less']
})
export class AddGoalComponent implements OnInit {
  interestingPlace: Event
  trip: Trip
  error: string | null
  participants: any[] = []
  goal: Goal

  constructor(private cookieService: CookieService,
  private router: Router,
		private cd: ChangeDetectorRef,
	  private tripService: TripService,
	  private goalService: GoalService,
      @Inject(POLYMORPHEUS_CONTEXT) private readonly context: TuiDialogContext<[Event, Trip], [Event, Trip, User[]]>,
		) {
	this.interestingPlace = this.context.data[0] /***/
	this.trip = this.context.data[1]
	this.participants = this.context.data[2]
	this.participants = this.participants.map((x: any) => {
		x['sum'] = 0
		return x
	})
	this.error = null
	let ownerId = -1
	if (cookieService.check('id'))
	{
		ownerId = Number(cookieService.get('id'))
	}
	this.goal = new Goal(null, 
		ownerId, 
		0,
		[],
		this.trip.tripId!
	)
  }

  deleteParticipant(user: User)
  {
	this.participants = this.participants.filter(x => x !== user)
  }

  ngOnInit(): void {
  }
  
  onSubmit()
  {
	this.setError(null)
	/*this.interestingPlace.amount = this.trip.participantsId.length + 1 - this.participants.length
	console.log(this.interestingPlace.amount)*/
	this.goal.amount = this.participants.map(x => x.sum).reduce((a, b) => a + b)
	this.goal.info = this.participants.map(x => new ParticipantInfo(x.userId ?? -1, x.sum, false))
	this.goalService.create(this.goal).subscribe(result => {
		console.log(result)
		if (!this.interestingPlace.goalId)
		{
			this.interestingPlace.goalId = []
		}
		if (!this.interestingPlace.goal)
		{
			this.interestingPlace.goal = []
		}
		this.interestingPlace.goalId.push(result.goalId!)
		this.interestingPlace.goal.push(result)
		this.trip.events.filter(x => x.latitude == this.interestingPlace.latitude && x.longitude == this.interestingPlace.longitude).
		forEach(x => {
			x.goalId = this.interestingPlace.goalId
			x.goal = this.interestingPlace.goal
		})
		this.tripService.update(this.trip).subscribe(result => this.onUpdate(result), error => this.setError('Something went wrong...'))
	})
  }

  onUpdate(result: any) {
	console.log(result)
	if (result == null)
	{
		this.setError('Something went wrong...')
	}
	else
	{
		this.context.completeWith([this.interestingPlace, result])
	}
  }
  
  setError(error: string | null) {
	this.error = error
	this.cd.detectChanges()
  }
  
  readonly editForm = new FormGroup({
	moneyControl: new FormControl(0, [
	    Validators.required,
		Validators.min(0)
	])
  })
}
