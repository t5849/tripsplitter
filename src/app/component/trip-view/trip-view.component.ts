import { ChangeDetectionStrategy, Component, OnInit, Inject, Injector } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { catchError, map } from 'rxjs/operators'
import { Router } from '@angular/router'

import { Trip } from '@app/model/trip'
import { Event } from '@app/model/event'
import { User } from '@app/model/user'
import { Goal } from '@app/model/goal'
import { FileInfo } from '@app/model/file-info'

import { UserService } from '@app/service/user.service'
import { TripService } from '@app/service/trip.service'
import { GoalService } from '@app/service/goal.service'

import {ChangeDetectorRef} from '@angular/core'
import { CookieService  } from 'ngx-cookie-service'

import {TuiDialogService} from '@taiga-ui/core'
import {PolymorpheusComponent} from '@tinkoff/ng-polymorpheus'

import { AddPoiComponent } from './add-poi/add-poi.component'
import { AddGoalComponent } from './add-goal/add-goal.component'

import { YaMapComponent, YaEvent, YaReadyEvent, YaGeocoderService } from 'angular8-yandex-maps'

import {FormControl, FormGroup, FormBuilder} from '@angular/forms'

import Places from './places.json';

import {TuiTime} from '@taiga-ui/cdk';

import { PoiService } from '@app/service/poi.service'
import { PoiType } from '@app/model/poi-type'

import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';


import {ViewChild, ElementRef} from '@angular/core';
import {TuiScrollbarComponent} from '@taiga-ui/core';

import $ from 'jquery';

declare var download: any;
declare var isGapiLoaded: any;
declare var test: any;


@Component({
  selector: 'app-trip-view',
  templateUrl: './trip-view.component.html',
  styleUrls: ['./trip-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class TripViewComponent implements OnInit {
	@ViewChild(TuiScrollbarComponent, {read: ElementRef})
    private readonly scrollBar?: ElementRef<HTMLElement>;
	
    trip: Trip | any
	owner: User | any
	users: User[] | any
	invitation: string
	
    activeItemIndex = 0
	
	public types: PoiType[] = []
	
	colors = [
	  '#8dda71',
	  '#9d6f64',
	  '#3682db',
	  '#34b41f',
	  '#e29398',
	  '#b8474e',
	  '#fcc068',
	  '#ff8a00',
	  '#dab3f9',
	  '#7b439e',
	  '#2fad96',
	  '#fcbb14',
	  '#ea97c4',
	  '#bd65a4',
	  '#7fd7cc',
	]
	
	getOptions(index: number)
	{
		return { 
			wayPointVisible: false, 
			zIndex: 1, 
			boundsAutoApply: false,
			routeActiveStrokeColor: this.colors[index], 
			routeVisible: false, 
			routeActiveVisible: true
		}
	}
	
	hoursLimit = 8
	divisions: any = []
	gropped_divisions: any = []
	
	save_to_db()
	{
		let events = []
		let curr_day = 0
		this.divisions.forEach((i: any, ind: number) => {
			if (this.is_event(i)) {
				i.day = this.get_curr_day(ind)
				i.order = this.get_event_index(i, this.get_curr_day(ind))
			}
		})
		this.trip.events = this.divisions.filter((i: any) => this.is_event(i))
		this.tripService.update(this.trip).subscribe(result => {
			this.cd.detectChanges()
		})
	}
	
	loaded = false
	load_from_db()
	{
		if (!this.loaded)
		{
			this.divisions = []
			this.append_day()
			if (this.trip.events.filter((x: Event) => x.day && x.day > 0).length == 0)
			{
				console.log('?')
				this.divisions = this.divisions.concat(this.trip.events)
			}
			else
			{
				const sorted_events = this.trip.events.sort(function (a: Event, b: Event) {  
					if (a.day == b.day) {
						return (a.order ?? 1) - (b.order ?? 1)
					}
					return (a.day ?? 1) - (b.day ?? 1)
				})
				let curr_day = 1
				sorted_events.forEach((e: Event) => {
					while (curr_day < (e.day ?? 1)) {
						this.append_day()
						curr_day = curr_day + 1
					}
					this.divisions.push(e)
				})
			}
		}
	}
	
	fill_divisions()
	{
		this.load_from_db()
		this.group_events_by_days()
	}
	append_day()
	{
		let max_day = Math.max.apply(Math, this.divisions.filter((x: any) => Number.isFinite(x)))
		this.divisions.push(Number.isFinite(max_day) ? max_day + 1 : 1)
	}
	is_event(t: any)
	{
		return typeof t !== 'number'
	}
	get_curr_day(pos: number)
	{
		for (let i = pos; i >= 0; i--)
		{
			if (Number.isFinite(this.divisions[i]))
			{
				return this.divisions[i]
			}
		}
		return -1
	}
	is_show_day(day: number)
	{
		return day > 0 && test.testForm2.get('_'+day).value
	}
	get_event_index(poi: Event, day: number)
	{
		const dayInd = this.divisions.indexOf(day)
		const poiInd = this.divisions.indexOf(poi)
		return poiInd - dayInd
	}
	
	group_events_by_days()
	{
		this.save_to_db()
		let res: any = []
		this.divisions.forEach((x: any, i: number) => {
			if (!res[this.get_curr_day(i) - 1]) {
				res[this.get_curr_day(i) - 1] = []
			}
			if (this.is_event(x))
			{
				res[this.get_curr_day(i) - 1].push(x)
			}
		})
		this.gropped_divisions = res
		this.trip.events.forEach((x: Event) => {
			this.load_content(x)
		})
	}
	
	append_day_btn()
	{
		this.append_day()
		
		setTimeout(() => {
			if (!this.scrollBar) {
				return;
			}
	 
			const {nativeElement} = this.scrollBar;
	 
			nativeElement.scrollTop = nativeElement.scrollHeight + 10
		}, 50)
	}
	
	drop(event: CdkDragDrop<string[]>) {
		if (event.currentIndex > 0)
		{
			moveItemInArray(this.divisions, event.previousIndex, event.currentIndex)
			this.group_events_by_days()
		}
	}
	
	suggest()
	{
		const param = this.trip.events.map((x: Event) => `${x.longitude},${x.latitude}`).join(';')
		const type = test.radioForm.get('testValue1').value == 'pedestrian' ? 'foot' : 'car'
	    const params = new HttpParams().append('waypoints', param).append('type', type)
		console.log(param)
		this.http.get<any[]>('https://traveling-together.hopto.org:8185/routes', {params}
		).subscribe(result => {
			console.log(result)
			
			this.divisions = []
			this.append_day()
			
			const first = result[0]
			const first_event = this.trip.events.filter((e: Event) => e.latitude == first.from.latitude && e.longitude == first.from.longitude)[0]
			this.divisions.push(first_event)
			let duration = this.parse_duration(first_event) * 60 // <- 60!
			
			result.forEach((x: any) => {
				const curr_event = this.trip.events.filter((e: Event) => e.latitude == x.to.latitude && e.longitude == x.to.longitude)[0]
				const curr_duration = x.duration + this.parse_duration(curr_event) * 60
				if ((duration + curr_duration) > this.hoursLimit * 60 * 60) {
					this.append_day()
					duration = curr_duration
				} else {
					duration = duration + curr_duration
				}
				this.divisions.push(curr_event)
			})
			this.group_events_by_days()
			console.log(this.divisions)
		})
	}
	
	durationPattern = /^([0-9]+)([mh])$/
	parse_duration(poi: Event)
	{
		if (poi.duration && poi.duration != "")
		{
			let num = Number(poi.duration.match(this.durationPattern)![1])
			const type = poi.duration.match(this.durationPattern)![2]
			if (type == 'h')
			{
				num *= 60
			}
			return num
		}
		return 0
	}
	
	public isDesktop = window.innerWidth > window.innerHeight
	onResize(e: any)
	{
	    this.isDesktop = window.innerWidth > window.innerHeight
		this.cd.detectChanges()
	}
	
	cache: any = []
	load_content(ev: Event)
	{	
		setTimeout(() => {
			if (!ev.fileSrcsReq)
			{
				ev.fileSrcsReq = 0
			}
			if (ev.fileIds && ev.fileSrcsReq < ev.fileIds.length && isGapiLoaded())
			{
				console.log('try!')
				ev.fileSrcsReq = ev.fileIds.length
				ev.fileSrcs = []
				ev.fileIds.forEach((x: FileInfo) => {
					const stored = this.cache.filter((o: any) => o.id == x.fileId)
					if (stored.length > 0)
					{
						ev.fileSrcs?.push(stored[0].value)
						this.cd.detectChanges()
						
					} else {
						download(x.fileId).then((z: string) => {
							download(x.previewId).then((y: string) => {
								const fileInfo = new FileInfo(z, y)
								this.cache.push({id: x.fileId, value: fileInfo})
								ev.fileSrcs?.push(fileInfo)
								this.cd.detectChanges()
							})
						})
					}
				})
			}
		}, 1000)
	}
	
	constructor(private http: HttpClient,
			  private router: Router, 
			private userService: UserService, 
		  private cd : ChangeDetectorRef, 
	    private cs: CookieService, 
	  private tripService: TripService, 
	  public poiService: PoiService, 
	  private goalService: GoalService,
	  private yaGeocoderService: YaGeocoderService,
		@Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
		@Inject(Injector) private injector: Injector,
	private fb:FormBuilder) {
		this.invitation = 'work in progres...'
		this.users = []
		
		let nav = this.router.getCurrentNavigation()
		if (nav)
		{
			this.trip = nav.extras.state
		}
		
		
		if (this.cs.check('requreReload'))
		{
			this.cs.delete('requreReload')
			if (this.trip)
			{
				cs.set('trip', String(this.trip.tripId))
			}
			location.reload()
		}
		
		if (this.trip)
		{
		    console.log(this.trip)
			tripService.getWeather(this.trip.latitude, this.trip.longitude).subscribe(result => {
				console.log(result)
			})
			if (!this.trip.events)
			{
				this.trip.events = []
			}
			this.tripService.getInviteByTripId(this.trip.tripId).subscribe(result => {
				this.invitation = 'http://traveling-together.hopto.org/#/trip-join/' + result.inviteId
				//this.invitation = 'http://localhost:4200/#/trip-join/' + result.inviteId
			})
			this.userService.getUserByUserId(Number(this.trip.ownerId)).subscribe(result => {
				this.owner = result
				this.cd.detectChanges()
			})
			this.trip.participantsId.map((x: number) => this.userService.getUserByUserId(x).subscribe(result => {
				console.log(result)
				this.users.push(result)
				this.getPayments()
				this.getDebts()
				this.cd.detectChanges()
			}))
			this.trip.events.forEach((x: Event) => {
				console.log(this.trip)
				if (x.goalId != null)
				{
					x.goal = []
					x.goalId.forEach((y: number) => {
						this.goalService.getGoalById(y).subscribe(result => {
							x.goal!.push(result)
							this.cd.detectChanges()
						})
					})
				}
			})
			this.fill_divisions()
			this.cd.detectChanges()
			cs.set('trip', String(this.trip.tripId))
		}
		else
		{
			if (cs.check('trip'))
			{
				this.tripService.getTripById(Number(cs.get('trip'))).subscribe(result => {
					this.trip = result
					console.log(this.trip)
					/*!!!*/
					if (!this.trip.events)
					{
						this.trip.events = []
					}
					this.tripService.getInviteByTripId(this.trip.tripId).subscribe(result => {
						this.invitation = 'http://traveling-together.hopto.org/#/trip-join/' + result.inviteId
						//this.invitation = 'http://localhost:4200/#/trip-join/' + result.inviteId
					})
					this.userService.getUserByUserId(Number(this.trip.ownerId)).subscribe(result => {
						this.owner = result
						this.cd.detectChanges()
					})
					this.trip.participantsId.map((x: number) => this.userService.getUserByUserId(x).subscribe(result => {
						this.users.push(result)
						this.getPayments()
						this.getDebts()
						this.cd.detectChanges()
					}))
					this.trip.events.forEach((x: Event) => {
						console.log(this.trip)
						this.load_content(x)
						if (x.goalId != null)
						{
							x.goal = []
							x.goalId.forEach((y: number) => {
								this.goalService.getGoalById(y).subscribe(result => {
									x.goal!.push(result)
									this.cd.detectChanges()
								})
							})
						}
					})
					this.fill_divisions()
					this.cd.detectChanges()
				})
			}
			else
			{
				console.log('!')
				console.log(this.trip)
				console.log(nav!.extras.state)
				this.router.navigate(['/home'])
			}
		}
		this.types = this.poiService.getPoiTypes()
	}
	
	allPlaces: Array<any>/* = Places;*/ = []
	setFilter(type: PoiType)
	{
		if (this.allPlaces.filter((x: any) => x.name == type.typeName).length > 0)
		{
			this.allPlaces = this.allPlaces.filter((x: any) => x.name != type.typeName)
			this.ym.target.geoObjects.each((x: any) => { 
				if (x.options._options.clusterIconColor == type.typeColor) {
					this.ym.target.geoObjects.remove(x)
				}
			})
		}
		else
		{
			if (Math.floor(this.trip.latitude) == 51 && Math.floor(this.trip.longitude) == 39)
			{
				this.allPlaces.push({
					name: type.typeName,
					color: type.typeColor,
					data: this.poiService.getPlacesVoronezh(type)
				})
			} 
			else
			{
				this.poiService.getPlacesOther(type, this.trip.latitude, this.trip.longitude).subscribe((result: any) => {
					console.log('fetch!')
					this.allPlaces.push({
						name: type.typeName,
						color: type.typeColor,
						data: result
					})
				})
			}
		}
		this.cd.detectChanges()
	}
	
	mapClick(e: YaEvent<ymaps.Map>)
	{
		const { target, event } = e;
		const coords = event.get('coords');
		
		this.yaGeocoderService.geocode(coords).subscribe((result: any) => {
			this.dialogService.open<[Event, Trip]>(new PolymorpheusComponent(AddPoiComponent, this.injector), {
				data: [new Event('', result.geoObjects.get(0).getAddressLine(), coords[0], coords[1], this.owner.userName), this.trip, false]
			}).subscribe({
				next: data => {
					const ans = data as [Event, Trip]
					if (ans)
					{
						this.trip = ans[1]
						this.trip.events.forEach((x: Event) => {
							console.log(this.trip)
							if (x.goalId != null)
							{
								x.goal = []
								x.goalId.forEach((y: number) => {
									this.goalService.getGoalById(y).subscribe(result => {
										x.goal!.push(result)
										this.cd.detectChanges()
									})
								})
							}
						})
						this.divisions.push(ans[0])
						this.group_events_by_days()
						this.cd.detectChanges()
						console.log(this.trip)
					}
				}
			})
		})
	}
	
	getBaloonContentLayout(place: any)
	{
		let component = this
		let contentLayoutBase = ymaps.templateLayoutFactory.createClass('').superclass
		return ymaps.templateLayoutFactory.createClass('<div>' +
			'<p>' + place.properties.description + '</p>' +
			'<button tuiButton id="travel-button" class="tui-form__button" type="submit" style="color: white; background: #526ed3; border-radius: 0.325rem; font: \"Manrope\", -apple-system, \"BlinkMacSystemFont\", system-ui, \"Roboto\", \"Segoe UI\", \"Helvetica Neue\", sans-serif; border: 0; outline: 0; user-select: none; appearance: none; padding: 0; border-color: white">Go here!</button>' +
			'</div>', {
				build: function () {
					contentLayoutBase.build.call(this)
					$('#travel-button').bind('click', this.onCounterClick)
				},
				clear: function () {
					$('#travel-button').unbind('click', this.onCounterClick)
					contentLayoutBase.clear.call(this)
				},
				onCounterClick: function () {
					component.goToPoi(place)
				}
			}
		)
	}
	
	getGalleryContentLayout(ev: Event)
	{
		if (this.ym)
		{
			let contentLayoutBase = ymaps.templateLayoutFactory.createClass('').superclass
			const id = "scrollable"
			return ymaps.templateLayoutFactory.createClass('<div id="' + id + '" style="display: flex; flex-flow: row;touch-action: none">' +
				(ev.fileSrcs?.map(x => {
					return "<img style='display:block; width:24rem;height:16rem;' src=" + x.fileId + " />"
				}).join() ?? '') +
				'<script>var item = document.getElementById("' + id + '").parentNode.parentNode;window.addEventListener("wheel", function (evt) {evt.preventDefault();item.scrollLeft += evt.deltaY;}, { passive: false });</script>'
				+ '</div>',
				{
					build: function () {
						contentLayoutBase.build.call(this)
					},
					clear: function () {
						contentLayoutBase.clear.call(this)
					}
				}
			)
		}
		return ""
	}
	
	getIconContentLaout(ev: Event)
	{
		if (this.ym)
		{
			let contentLayoutBase = ymaps.templateLayoutFactory.createClass('').superclass
			const ind = this.get_curr_day(this.divisions.findIndex((x: Event) => x.eventId == ev.eventId))
			return ymaps.templateLayoutFactory.createClass('<div><div style="color: white;background: ' + this.colors[ind] + ';text-align: center;border-radius: 0.25rem;padding: 0.4rem;min-width: 2rem; max-width: 2rem; min-height: 2rem; max-height: 2rem; display: flex; flex-flow: row;">'+
				(ev.fileSrcs?.slice(0,1).map(x => {
					return "<img style='display:block; width:32px;height:32px;margin: auto;' src=" + x.previewId + " />"
				}).join('') ?? '')
			  + '</div><div style="display: inline-block;position: relative; top: -3.5rem; right: -2.5rem;height: 1rem;width: 1rem;border-radius: 50%;background: blue;color: white;font: \"Manrope\", -apple-system, \"BlinkMacSystemFont\", system-ui, \"Roboto\", \"Segoe UI\", \"Helvetica Neue\", sans-serif; font-weight: bold;">' + String(ev.fileSrcs?.length ?? 0) + '</div>',
				{
					build: function () {
						contentLayoutBase.build.call(this)
					},
					clear: function () {
						contentLayoutBase.clear.call(this)
					}
				}
			)
		}
		return ""
	}
	
	goToPoi(place: any)
	{
		this.dialogService.open<[Event, Trip]>(new PolymorpheusComponent(AddPoiComponent, this.injector), {
			data: [new Event(place.properties.name, place.properties.description, place.geometry.coordinates[1], place.geometry.coordinates[0], this.owner.userName), this.trip, false]
		}).subscribe({
			next: data => {
				const ans = data as [Event, Trip]
				console.log(ans)
				if (ans)
				{
					this.trip = ans[1]
					this.trip.events.forEach((x: Event) => {
						console.log(this.trip)
						if (x.goalId != null)
						{
							x.goal = []
							x.goalId.forEach((y: number) => {
								this.goalService.getGoalById(y).subscribe(result => {
									x.goal!.push(result)
									this.cd.detectChanges()
								})
							})
						}
					})
					this.divisions.push(ans[0])
					this.group_events_by_days()
					this.cd.detectChanges()
				}
            }
		})
	}
	
	getPoiGeos(events: Event[]) : ymaps.IMultiRouteReferencePoint[]
	{
		return events.map((y: any) => [y.latitude, y.longitude])
	}
	
	getPicturedEvents(collection: any)
	{
		return collection.filter((x: Event) => {
			const ind = this.get_curr_day(this.divisions.findIndex((y: Event) => x.eventId == y.eventId))
			return x.fileSrcs && x.fileSrcs[0] && this.is_show_day(ind)
		})
	}
	
	editPoi(poi: Event, tab = 0)
	{
		this.dialogService.open<[Event, Trip, boolean]>(new PolymorpheusComponent(AddPoiComponent, this.injector), {
			data: [this.trip.events.filter((x: Event) => x.eventId == poi.eventId)[0], this.trip, true, tab]
		}).subscribe(result => {
			this.trip = result[1]
			if (result[2] == true)
			{
				this.divisions = this.divisions.filter((x: any) => !this.is_event(x) || (x.eventId != result[0].eventId))
			}
			else
			{
				this.divisions = this.divisions.map((x: any) => this.is_event(x) && x.eventId == result[0].eventId ? result[0] : x)
			}
			this.group_events_by_days()
			this.cd.detectChanges()
		})
	}
	
	addGoal(poi: Event)
	{
		this.dialogService.open<[Event, Trip]>(new PolymorpheusComponent(AddGoalComponent, this.injector), {
			data: [poi, this.trip, this.users.concat(this.owner)]
		}).subscribe(result => {
			this.getPayments()
			this.getDebts()
			this.cd.detectChanges()
		})
	}
	
	contribute(paymentInfo: any)
	{
		if (this.cs.check('id')){
			const receive =  paymentInfo[0].qiwiName
			const tripId = this.trip.tripId
			const senderId = Number(this.cs.get('id'))
			const recipientId = paymentInfo[0].userId
			const sum = -paymentInfo[1]
			this.goalService.doPayment(receive, tripId, senderId, recipientId, sum).subscribe(result => {
				console.log(result)
				window.open(result)
			})
		}
		/*this.payments = this.payments.filter((x: any) => x != paymentInfo)
	4100117620712227	window.open(`https://yoomoney.ru/quickpay/confirm.xml?receiver=${paymentInfo[0].qiwiName}&targets=test&quickpay-form=donate&sum=${-paymentInfo[1]}`)*/
	}
	
	deleteTrip()
	{
		this.tripService.delete(this.trip).subscribe(result => {
			this.router.navigate(['/home'])
		}, error => {
			//this.router.navigate(['/home2'])
		})
	}
	
	revokeInvitation()
	{
		this.tripService.revoke(this.trip.tripId).subscribe(result => {
			this.invitation = 'http://traveling-together.hopto.org/#/trip-join/' + result.inviteId
			//this.invitation = 'http://localhost:4200/#/trip-join/' + result.inviteId
		})
	}
    
	ym: any = null
	
	toggledInfo = false
	toggleInfo() {
		this.toggledInfo = !this.toggledInfo
	}
	
	onMapReady(event: YaReadyEvent<ymaps.Map>): void {
		const { ymaps, target } = event
		console.log(event)
		this.ym = event
	}
  
    getPaidParticipants(goal: Goal)
	{
		return goal.info.filter(x => x.paid)
	}
  
    getContribute(goal: Goal)
	{
		return goal.amount / goal.info.length
	}
 
	payments: any = []
	paymentsLock = false
	getPayments()
	{
		if (!this.paymentsLock)
		{
			this.paymentsLock = true
			this.payments = []
			if (this.cs.check('id'))
			{
				const id = Number(this.cs.get('id'))
				this.http.get<any[]>('https://traveling-together.hopto.org:8181/goals/info?tripId=' + this.trip.tripId + '&userId=' + id).subscribe(result => {
					let count = 0
					for (const [key, value] of Object.entries(result)) {
						this.userService.getUserByUserId(Number(key)).subscribe(y => {
							const sum = value
							if (sum < 0)
							{
								console.log([y, sum])
								this.payments.push([y, sum])
								this.cd.detectChanges()
							}
							count = count + 1
							if (count == Object.entries(result).length)
							{
								console.log('unlock payments')
								this.paymentsLock = false
							}
						})
					}
				})
			}
		}
	}
 
	debts: any = []
	debtsLock = false
	getDebts()
	{
		if (!this.debtsLock)
		{
			this.debtsLock = true
			this.debts = []
			if (this.cs.check('id'))
			{
				const id = Number(this.cs.get('id'))
				this.http.get<any[]>('https://traveling-together.hopto.org:8181/goals/info?tripId=' + this.trip.tripId + '&userId=' + id).subscribe(result => {
					let count = 0
					for (const [key, value] of Object.entries(result)) {
						this.userService.getUserByUserId(Number(key)).subscribe(y => {
							const sum = value
							if (sum > 0)
							{
								console.log([y, sum])
								this.debts.push([y, sum])
								this.cd.detectChanges()
							}
							count = count + 1
							if (count == Object.entries(result).length)
							{
								console.log('unlock debts')
								this.debtsLock = false
							}
						})
					}
				})
			}
		}
	}
	
	parse_time(poi: Event)
    {
		let duration = 0
		if (poi.duration && poi.duration != "")
		{
			duration = Number(poi.duration.match(this.durationPattern)![1])
			const type = poi.duration.match(this.durationPattern)![2]
			if (type == 'h')
			{
				duration *= 60
			}
		}
		const time = new TuiTime(Math.floor(duration/60), duration % 60)
		const h = time.hours.toString().length > 1 ? time.hours.toString() : '0'+time.hours.toString()
		const m = time.minutes.toString().length > 1 ? time.minutes.toString() : '0'+time.minutes.toString()
		return [h, m]
    }
	
    items = ['auto', 'masstransit', 'pedestrian']
    radioForm = new FormGroup({
        testValue1: new FormControl(this.items[0]),
    })
	
	readonly contributeForm = this.fb.group({})
	
	readonly testForm = new FormGroup({
		red: new FormControl(false),
		green: new FormControl(false),
		blue: new FormControl(false),
		black: new FormControl(false),
		white: new FormControl(false),
		yellow: new FormControl(false),
		pink: new FormControl(false),
		gray: new FormControl(false),
		cyan: new FormControl(false),
		orange: new FormControl(false),
		magenta: new FormControl(false),
		brown: new FormControl(false),
	})
	
	readonly testForm2 = new FormGroup({
		_0: new FormControl(true),
		_1: new FormControl(true),
		_2: new FormControl(true),
		_3: new FormControl(true),
		_4: new FormControl(true),
		_5: new FormControl(true),
		_6: new FormControl(true),
		_7: new FormControl(true),
		_8: new FormControl(true),
		_9: new FormControl(true),
		_10: new FormControl(true),
		_11: new FormControl(true),
		_12: new FormControl(true),
		_13: new FormControl(true),
		_14: new FormControl(true),
		_15: new FormControl(true),
		_16: new FormControl(true),
		_17: new FormControl(true),
		_18: new FormControl(true),
		_19: new FormControl(true),
	})
	
	ngOnInit(){
      (globalThis as any).test = this
	   window.addEventListener("wheel", function (evt) {
						const item: any = document.getElementById("scrollable")?.parentNode?.parentNode;
						if (evt && item)
						{
							evt.preventDefault();
							item.scrollLeft += evt.deltaY;
						}
		}, { passive: false });
	}
	
	tmp: any;
	qwerty()
	{
		console.log($('tui-toggle tui-wrapper'))
		$('tui-toggle tui-wrapper').each((i: any, x: any) => {
			console.log(x)
			x.setAttribute('data-appearance', 'custom')
			this.tmp = x
		})
	}
	
	ngAfterViewChecked() {
		if (this.ym)
		{
			$('[class*=transport-pin_tail_pedestrian]').hide()
		}
		$('tui-toggle tui-wrapper').each((i: any, x: any) => {
			if (x.getAttribute('data-appearance') == 'primary') {
				x.setAttribute('data-appearance', 'custom')
			}
		})
	}
}
