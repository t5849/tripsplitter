import { Component, OnInit, Inject } from '@angular/core'
import {FormControl, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors} from '@angular/forms'

import { Router } from '@angular/router'

import { Trip } from '@app/model/trip'
import { Event } from '@app/model/event'
import { FileInfo } from '@app/model/file-info'

import { UserService } from '@app/service/user.service'
import { TripService } from '@app/service/trip.service'

import {tuiCreateTimePeriods} from '@taiga-ui/kit';
import {TuiTime} from '@taiga-ui/cdk';

import { ChangeDetectorRef } from '@angular/core'

import {TuiDialogContext} from '@taiga-ui/core'
import {POLYMORPHEUS_CONTEXT} from '@tinkoff/ng-polymorpheus';

  
const durationPattern = /^[0-9]+[mh]$/

declare var createEmptyFile: any;
declare var uploadFile: any;
declare var share: any;
declare var isLoggedIn: any;
//declare var gfile: any;

@Component({
  selector: 'app-add-poi',
  templateUrl: './add-poi.component.html',
  styleUrls: ['./add-poi.component.less']
})
export class AddPoiComponent implements OnInit {
  interestingPlace: Event
  trip: Trip
  error: string | null
  edit: boolean
  
  fileProgress = 0
  activeItemIndex = 0
  infos: any = []
  
  files: File | null = null;
  
  resizeImage(datas: string, wantedWidth: number, wantedHeight: number){
    return new Promise(async function(resolve,reject){
        var img = document.createElement('img');
        img.onload = function()
        {
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            canvas.width = wantedWidth;
            canvas.height = wantedHeight;
            ctx?.drawImage(img, 0, 0, wantedWidth, wantedHeight);
            var dataURI = canvas.toDataURL();
            resolve(dataURI);
        };
        img.src = datas;
    })
  }
  
  items1 = tuiCreateTimePeriods();
  
  durationPattern = /^([0-9]+)([mh])$/
  parse_duration(poi: Event)
  {
	let duration = 30
	if (poi.duration && poi.duration != "")
	{
		duration = Number(poi.duration.match(this.durationPattern)![1])
		const type = poi.duration.match(this.durationPattern)![2]
		if (type == 'h')
		{
			duration *= 60
		}
	}
	return new TuiTime(Math.floor(duration/60), duration % 60)
  }
  
  
  constructor(private router: Router,
		private cd: ChangeDetectorRef,
	  private tripService: TripService,
			private userService: UserService, 
      @Inject(POLYMORPHEUS_CONTEXT) private readonly context: TuiDialogContext<[Event, Trip, boolean], [Event, Trip, boolean, number]>,
		) 
  {
	this.interestingPlace = this.context.data[0]
	this.trip = this.context.data[1]
	this.edit = this.context.data[2]
	this.activeItemIndex = this.context.data[3] ?? 0
	this.error = null
	
	const time = this.parse_duration(this.interestingPlace);
	console.log(time);
	(this.editForm.get('placeDurationControl') as any)["value" as any] = time;
		
	if (this.edit && this.interestingPlace.goal)
	{
		this.infos = this.interestingPlace.goal.map(g => {
			return {
				data: g.info.map(x => [x]),
				amount: g.amount
			}
		})
		this.infos.forEach((x: any) => {
			x.data.forEach((y: any) => {
				this.userService.getUserByUserId(Number(y[0].participantId)).subscribe(result => {
					y.push(result.userName)
					this.cd.detectChanges()
				})
			})
		})
		/*console.log(this.editForm.get('placeDurationControl')!.value)
		console.log(new TuiTime(0, 50))*/
	}
  }
  
  isLogged()
  {
	return isLoggedIn()
  }
  
  private file: File | null = null
  attached(f: File)
  {
	this.file = f
  }
  
  delete()
  {
	console.log(this.trip)
	console.log(this.interestingPlace.eventId)
	this.tripService.deleteEvent(this.trip, this.interestingPlace.eventId!).subscribe(result => 
		this.onUpdate(result, true), error => this.setError('Something went wrong...'))
  }
  
  uploadFile(callback: any)
  {
    if (this.edit)
	{
		if (this.files) {
			const gfile = this.files
			console.log(gfile)
			let reader = new FileReader();
				reader.readAsDataURL(gfile);
				let resizeImage = this.resizeImage
				const setProgress = (x: number) => {
					this.fileProgress = x
					this.cd.detectChanges()
				}
				setProgress(1)
				reader.onload = function () {
					setProgress(5)
			        resizeImage(<string>reader.result, 64, 64).then((o: any) => {
					setProgress(10)
					createEmptyFile('test').then((x: any) => {
									setProgress(20)
									uploadFile(x, reader.result).then((y: any) => {
										setProgress(50)
										share(x).then((z: any) => {
											setProgress(60)
											createEmptyFile('testPreview').then((x1: any) => {
												setProgress(70)
												uploadFile(x1, o).then((y1: any) => {
													setProgress(95)
													share(x1).then((z1: any) => {
														setProgress(100)
														callback(x, x1)
													})
												});
											});
										})
									});
								});
					})
				};
				reader.onerror = function (error) {
					console.log('Error: ', error);
				};
		} else {
			callback(null)
		}
	} else {
		callback(null)
	}
  }

  ngOnInit(): void {
  }
  
  onSubmit()
  {
	console.log(this.interestingPlace)
	this.setError(null)
	this.update()
  }

  update()
  {
	this.uploadFile((fileId: string, previewId: string) => {
		if (!this.interestingPlace.fileIds)
		{
			this.interestingPlace.fileIds = []
		}
		if (fileId && previewId)
		{
			this.interestingPlace.fileIds.push(new FileInfo(fileId, previewId))
		}
		if (!this.trip.events.includes(this.interestingPlace) && !this.edit)
		{
			this.trip.events.push(this.interestingPlace)
			console.log(this.interestingPlace)
		}
		console.log(this.trip)
		const time = this.editForm.get('placeDurationControl')!.value;
		const m = time.hours * 60 + time.minutes;
		this.interestingPlace.duration = m.toString() + 'm';
		this.tripService.update(this.trip).subscribe(result => this.onUpdate(result, false), error => this.setError('Something went wrong...'))
	})
  }

  onUpdate(result: any, deleted: boolean) {
	console.log(result)
	if (result == null)
	{
		this.setError('Something went wrong...')
	}
	else
	{
		if (!this.edit)
		{
			this.context.completeWith([result.events[result.events.length - 1], result, false])
		}
		else
		{
			this.context.completeWith([this.interestingPlace, result, deleted])
		}
	}
  }
  
  setError(error: string | null) {
	if (!this.edit)
	{
		this.trip.events = this.trip.events.filter(x => 
		x.latitude != this.interestingPlace.latitude ||
		x.longitude != this.interestingPlace.longitude
	)
	}
	this.error = error
	this.cd.detectChanges()
  }
  
  checkPlaceName: ValidatorFn = (field: AbstractControl):  ValidationErrors | null => {
    let name = field.value
	if (name?.length < 3 || name?.length > 127) {
		return {
			lengthError: 'Place name length should be between 3 and 127'
		}
	}
	return null
  }
  
  checkPlaceDuration: ValidatorFn = (field: AbstractControl):  ValidationErrors | null => {
    let val = field.value
	if (val.length < 1)
	{
		return null
	}
	return durationPattern.test(val) ? null : {
	  durationError: 'Duration field should contains number and time format like 2h or 45m'
	}
  }
  
  readonly editForm = new FormGroup({
    placeNameControl: new FormControl('', [
	    Validators.required,
		this.checkPlaceName
	]),
	placeDescControl: new FormControl(),
	filesControl: new FormControl(),
	placeLatitudeControl: new FormControl(),
	placeLongitudeControl: new FormControl(),
	placeDurationControl: new FormControl(new TuiTime(0, 30), [
		//this.checkPlaceDuration
	])
  })
}
