import { Component, OnInit, Inject, Injector } from '@angular/core'
import { UserService } from '@app/service/user.service'
import { TripService } from '@app/service/trip.service'
import { User } from '@app/model/user'
import { Trip } from '@app/model/trip'
import { TripInfo } from '@app/model/trip-info'

import { CookieService  } from 'ngx-cookie-service'

import {ChangeDetectorRef} from '@angular/core'

import {FormControl, FormGroup} from '@angular/forms'


import { EditUsernameComponent } from './edit-username/edit-username.component'
import { EditQiwinameComponent } from './edit-qiwiname/edit-qiwiname.component'
import { ChangePasswordComponent } from './change-password/change-password.component'

import {TuiDialogService} from '@taiga-ui/core'
import {PolymorpheusComponent} from '@tinkoff/ng-polymorpheus'

import {TuiNotificationsService} from '@taiga-ui/core';

import {NgbModal, ModalDismissReasons} 
      from '@ng-bootstrap/ng-bootstrap';


import $ from 'jquery';


declare var logIn: any;
declare var logOut: any;
declare var isLoggedIn: any;

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.less'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class UserInfoComponent implements OnInit {
  checkForm = new FormGroup({
	ownTrips: new FormControl(false)
  })
  
  user: User | null
  trips: TripInfo[]
  
  public isDesktop = window.innerWidth > window.innerHeight
  onResize(e: any)
  {
	this.isDesktop = window.innerWidth > window.innerHeight
	this.cd.detectChanges()
  }
  
  copy(tripInfo: TripInfo)
  {
	  const url = `http://traveling-together.hopto.org/#/trip-join/${tripInfo.invite.inviteId}`;
	  //const url = `http://localhost:4200/#/trip-join/${tripInfo.invite.inviteId}`;
	  navigator.clipboard.writeText(url);
	 /* this.notificationsService
           .show('', {
               label: 'Join link copyed in clipboard!',
           })
           .subscribe();*/
	  console.log(($('#_'+tripInfo.trip.tripId) as any)[0].classList);
	  ($('#_'+tripInfo.trip.tripId) as any)[0].classList.add('visible')
	  setTimeout(() => {
	  ($('#_'+tripInfo.trip.tripId) as any)[0].classList.remove('visible')
	  }, 350)
  }
  
  modalReference: any = null;
  canClose = false
  open(content: any) {
    if (this.modalReference == null)
	{
		this.modalReference = this.modalService.open(content,{ariaLabelledBy: 'modal-basic-title'})
	}
  }
  close(event: any)
  {
	if (this.modalReference != null)
	{
		if (this.canClose)
		{
			this.canClose = false
			this.modalReference.close()
			this.modalReference = null
		} else 
		{
			this.canClose = true
		}
	}
  }
  
  constructor(private tripService: TripService, 
			private cookieService: CookieService, 
		  private userService: UserService, 
		private cd : ChangeDetectorRef, 
		@Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
		@Inject(Injector) private injector: Injector,
		@Inject(TuiNotificationsService)
        private readonly notificationsService: TuiNotificationsService,
		private modalService: NgbModal
		
	) {
	this.user = null
	this.trips = []
	if (cookieService.check('id'))
	{
		this.userService.getUserByUserId(Number(cookieService.get('id'))).subscribe(result => {
			const userId = result.userId ?? 0
			this.user = result
			this.cd.detectChanges()
			this.tripService.getTrips().subscribe(result => {
				const ts = result.filter(x => x.ownerId == userId || x.participantsId.includes(userId))
				this.userService.getUserNamesByUserIds(ts.map(x => x.ownerId)).subscribe(users => {	
					console.log(users)
					this.tripService.getInvitesByTripIds(ts.map(x => x.tripId!)).subscribe(invites => {	
						console.log(invites)
						ts.forEach((e, i) => {
							this.trips.push(new TripInfo(e, users[i], invites[i]))
							this.cd.detectChanges()
						})
					})
				})
				this.cd.detectChanges()
				/*trips.map(x => this.userService.getUserByUserId(x.ownerId).subscribe(result => {
					this.trips.push(new TripInfo(x, result))
					this.cd.detectChanges()
				}))*/
			})
		})
	}
	console.log(this.checkForm.get('ownTrips')?.value)
  }

  showEditUserNameDialog() {
		console.log(this.injector)
	if (this.user)
	{
		this.dialogService.open(new PolymorpheusComponent(EditUsernameComponent, this.injector), {
			data: this.user
		}).subscribe()
	}
  }

  showChangePasswordDialog() {
		console.log(this.injector)
	if (this.user)
	{
		this.dialogService.open(new PolymorpheusComponent(ChangePasswordComponent, this.injector), {
			data: this.user
		}).subscribe()
	}
  }
  
  isLogged()
  {
	return isLoggedIn()
  }
  
  googleLogIn()
  {
	if (!isLoggedIn())
	{
	  logIn()
	} else
	{
	  logOut()
	}
  }
  
  showEditQiwiNameDialog() {
	if (this.user)
	{
		this.dialogService.open(new PolymorpheusComponent(EditQiwinameComponent, this.injector), {
			data: this.user
		}).subscribe()
	}
  }

  ngOnInit(): void {
  }
}
