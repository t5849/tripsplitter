import { Component, OnInit, Inject } from '@angular/core'
import {FormControl, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors} from '@angular/forms'

import { ActivatedRoute, Router } from '@angular/router'
import { User } from '@app/model/user'
import { UserService } from '@app/service/user.service'

import { ChangeDetectorRef } from '@angular/core'

import {TuiDialogContext} from '@taiga-ui/core'
import {POLYMORPHEUS_CONTEXT} from '@tinkoff/ng-polymorpheus';

const latinChars = /^[a-zA-Z0-9 ]+$/


@Component({
  selector: 'app-edit-qiwiname',
  templateUrl: './edit-qiwiname.component.html',
  styleUrls: ['./edit-qiwiname.component.less']
})
export class EditQiwinameComponent implements OnInit {
  user: User
  error: string | null

  constructor(private router: Router, 
			private userService: UserService, 
		private cd: ChangeDetectorRef,
      @Inject(POLYMORPHEUS_CONTEXT) private readonly context: TuiDialogContext<never, User>,
		) {
	this.user = {...this.context.data}
	this.error = null
  }

  ngOnInit(): void {
  }
  
  onSubmit()
  {
	this.setError(null)
	this.userService.update(this.user).subscribe(result => this.onUpdate(result), error => this.setError('Something went wrong...'))
  }

  onUpdate(result: any) {
	console.log(result)
	if (result == null)
	{
		this.setError('Something went wrong...')
	}
	else
	{
		window.location.reload();
	}
  }
  
  setError(error: string | null) {
	this.error = error
	this.cd.detectChanges()
  }
  
  checkQiwiName: ValidatorFn = (field: AbstractControl):  ValidationErrors | null => {
      let qiwiname = field.root.get('qiwiNameControl')?.value
	  return latinChars.test(qiwiname) ? null : {
		  latinError: 'A qiwi nickname should contains latin letters, spaces or numbers only'
	  }
  }
  
  readonly editForm = new FormGroup({
    qiwiNameControl: new FormControl('', [
	    Validators.required,
		this.checkQiwiName
	]),
  })
}
