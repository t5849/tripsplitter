import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditQiwinameComponent } from './edit-qiwiname.component';

describe('EditQiwinameComponent', () => {
  let component: EditQiwinameComponent;
  let fixture: ComponentFixture<EditQiwinameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditQiwinameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditQiwinameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
