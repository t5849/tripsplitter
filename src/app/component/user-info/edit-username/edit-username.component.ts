import { Component, OnInit, Inject } from '@angular/core'
import {FormControl, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors} from '@angular/forms'

import { ActivatedRoute, Router } from '@angular/router'
import { User } from '@app/model/user'
import { UserService } from '@app/service/user.service'

import { CookieService  } from 'ngx-cookie-service'

import { ChangeDetectorRef } from '@angular/core'

import {TuiDialogContext} from '@taiga-ui/core'
import {POLYMORPHEUS_CONTEXT} from '@tinkoff/ng-polymorpheus';

const latinChars = /^[a-zA-Z ]+$/

@Component({
  selector: 'app-edit-username',
  templateUrl: './edit-username.component.html',
  styleUrls: ['./edit-username.component.less']
})
export class EditUsernameComponent implements OnInit {
  user: User
  error: string | null

  constructor(private router: Router, 
			private userService: UserService, 
		  private cookieService: CookieService, 
		private cd: ChangeDetectorRef,
      @Inject(POLYMORPHEUS_CONTEXT) private readonly context: TuiDialogContext<never, User>,
		) {
	this.user = {...this.context.data}
	this.error = null
  }

  ngOnInit(): void {
  }
  
  onSubmit()
  {
	this.setError(null)
	this.userService.update(this.user).subscribe(result => this.onUpdate(result), error => this.setError('Something went wrong...'))
  }

  onUpdate(result: any) {
	console.log(result)
	if (result == null)
	{
		this.setError('Something went wrong...')
	}
	else
	{
		window.location.reload();
	}
  }
  
  setError(error: string | null) {
	this.error = error
	this.cd.detectChanges()
  }
  
  checkUserName: ValidatorFn = (field: AbstractControl):  ValidationErrors | null => {
      let username = field.root.get('userNameControl')?.value
	  return latinChars.test(username) ? null : {
		  latinError: 'Username should contains latin letters only'
	  }
  }
  
  readonly editForm = new FormGroup({
    userNameControl: new FormControl('', [
	    Validators.required,
		this.checkUserName
	]),
  })
}
