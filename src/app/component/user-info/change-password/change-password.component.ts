import { Component, OnInit, Inject } from '@angular/core'
import {FormControl, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors} from '@angular/forms'

import { ActivatedRoute, Router } from '@angular/router'
import { User } from '@app/model/user'
import { AlterPasswordUser } from '@app/model/alter-password-user'
import { UserService } from '@app/service/user.service'

import { CookieService  } from 'ngx-cookie-service'

import { ChangeDetectorRef } from '@angular/core'

import {TuiDialogContext} from '@taiga-ui/core'
import {POLYMORPHEUS_CONTEXT} from '@tinkoff/ng-polymorpheus';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.less']
})
export class ChangePasswordComponent implements OnInit {
  user: AlterPasswordUser
  error: string | null

  constructor(private router: Router, 
			private userService: UserService, 
		  private cookieService: CookieService, 
		private cd: ChangeDetectorRef,
      @Inject(POLYMORPHEUS_CONTEXT) private readonly context: TuiDialogContext<never, User>,
		) {
	this.user = new AlterPasswordUser(this.context.data.userId ?? 0, this.cookieService.get('access'), '')
	this.error = null
  }

  ngOnInit(): void {
  }
  
  onSubmit()
  {
	this.setError(null)
	this.userService.changePassword(this.user).subscribe(result => this.onUpdate(result), error => this.setError('Something went wrong...'))
  }

  onUpdate(result: any) {
	console.log(result)
	if (result == null)
	{
		this.setError('Something went wrong...')
	}
	else
	{
		window.location.reload();
	}
  }
  
  setError(error: string | null) {
	this.error = error
	this.cd.detectChanges()
  }
  
  checkPasswords: ValidatorFn = (field: AbstractControl):  ValidationErrors | null => { 
	if (!this.checkPasswordLength(field)) {
		return {
			lengthError: 'Password\'s length should be between 6 and 18'
		}
	}
	this.checkPasswordConfirmation(field)
	return null
  }
  
  checkPasswordsConfirm: ValidatorFn = (field: AbstractControl):  ValidationErrors | null => { 
	if (!this.checkPasswordLength(field)) {
		return {
			lengthError: 'Password\'s length should be between 6 and 18'
		}
	}
	if (!this.checkPasswordConfirmation(field)) {
		return {
			confirmError: 'Passwords should be same!'
		}
	}
	return null
  }
  
  checkPasswordLength(field: AbstractControl) : boolean {
	let value = field.value
	return value?.length > 5 && value?.length < 19
  }
  
  checkPasswordConfirmation(field: AbstractControl) : boolean {
	let passControl = field.root.get('passwordControl')
	let confirmPassControl = field.root.get('passwordConfirmControl')
	let pass = passControl?.value
    let confirmPass = confirmPassControl?.value
	if (passControl !== null && confirmPassControl !== null) {
	    let err = pass !== confirmPass ? {
		    confirmError: 'Passwords should be same'
	    } : null
		confirmPassControl.setErrors(err)
		return err === null
    }
	return false
  }
  
  readonly editForm = new FormGroup({
    passwordControl: new FormControl('', [
		this.checkPasswords
	]),
    passwordConfirmControl: new FormControl('', [
		this.checkPasswordsConfirm
	]),
  })
}
