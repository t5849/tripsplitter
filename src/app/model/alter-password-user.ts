export class AlterPasswordUser {
	userId: number;
	token: string;
	password: string;
	
	constructor(userId: number, token: string, password: string){
		this.userId = userId
		this.token = token
		this.password = password
	}
}
