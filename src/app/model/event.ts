import { Goal } from '@app/model/goal'
import { FileInfo } from '@app/model/file-info'

export class Event {
	constructor(
	    public name: string,
		public desc: string,
		public latitude: number,
		public longitude: number,
		public creator?: string,
		public duration?: string,
		public goalId?: number[],
		public goal?: Goal[],
		public money?: number,
		public amount?: number,
		public fileId?: string,
		public fileSrc?: string,
		public fileIds?: Array<FileInfo>,
		public fileSrcs?: Array<FileInfo>,
	    public eventId: number | null = null,
		public fileSrcsReq: number = 0,
		public day?: number,
		public order?: number
		) {
		if (!duration)
		{
			this.duration = ''
		}
		this.money = -1
		this.amount = 0
		if (!this.day || this.day == 0)
		{
			this.day = 1
		}
		if (!this.order || this.order == 0)
		{
			this.order = 1
		}
	}
}
