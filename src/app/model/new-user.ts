import { User } from './user';

export class NewUser {
	user: User;
	password: string;
	
	constructor(user: User, password: string){
		this.user = user;
		this.password = password;
	}
}
