import { User } from './user'

export class ApprovedUser {
	user: User
	accessToken: string
	
	constructor(user: User, accessToken: string){
		this.user = user
		this.accessToken = accessToken
	}
}
