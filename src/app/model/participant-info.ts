export class ParticipantInfo {
	constructor(
		public participantId: number,
		public sum: number,
		public paid: boolean
	)
	{
		
	}
}

