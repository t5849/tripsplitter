import { ParticipantInfo } from '@app/model/participant-info'

export class Goal {
	constructor(public goalId: number | null,
		public ownerId: number,
		public amount: number,
		public info: ParticipantInfo[],
		public tripId: number,
		public isPaid?: boolean
	)
	{
		if (!this.isPaid) {
			this.isPaid = false
		}
	}
}
