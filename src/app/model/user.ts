export class User {
	userId: number | null
	userName: string
	userMail: string
	qiwiName: string
	
	constructor(){
		this.userId = null
		this.userName = ''
		this.userMail = ''
		this.qiwiName = ''
	}
}
