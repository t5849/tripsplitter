import { CookieService  } from 'ngx-cookie-service'

import { Event } from '@app/model/event'

export class Trip {
	tripId: number | null
	name: string
	ownerId: number
	latitude: number
	longitude: number
	participantsId: number[]
	events: Event[]
	
	constructor() {
		this.tripId = null
		this.name = ''
		this.ownerId = 0
		this.latitude = 0
		this.longitude = 0
		this.participantsId = []
		this.events = []
	}
}
