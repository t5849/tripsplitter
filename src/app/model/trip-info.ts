import { User } from '@app/model/user'
import { Trip } from '@app/model/trip'
import { Invite } from '@app/model/invite'

export class TripInfo {
	trip: Trip
	owner: User
	invite: Invite
	
	constructor(trip: Trip, owner: User, invite: Invite) {
		this.trip = trip
		this.owner = owner
		this.invite = invite
	}
}
