export class Subject {
	constructor(
		public lat: number,
		public lon: number,
		public name: string,
		public desc: string,
		public pic: string,
	)
	{
		
	}
}
