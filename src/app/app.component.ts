import {ChangeDetectionStrategy, Component } from '@angular/core'
import {TUI_VALIDATION_ERRORS} from '@taiga-ui/kit'

import { TripService } from '@app/service/trip.service'
import { UserService } from '@app/service/user.service'
import { CookieService  } from 'ngx-cookie-service'

import {ChangeDetectorRef} from '@angular/core'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less', './weather-icons.min.css' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
        provide: TUI_VALIDATION_ERRORS,
        useValue: {
			required: 'This field should be filled',
            email: 'Email should be a valid email address'
        },
    },
  ],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class AppComponent {
  readonly title = 'TravelingTogether'
  userName: string | null
  maxHeight: number
  weather: any = null
  
  icons: any = {
	'2': 'wi-thunderstorm',
	'3': 'wi-rain-mix',
	'5': 'wi-rain',
	'6': 'wi-snow',
	'7': 'wi-cloudy',
	'8': 'wi-day-cloudy',
  }
  
  getIcon(weather: any)
  {
	const codeDiv = Math.round(weather.id / 100).toString()
	if (codeDiv in this.icons)
	{
		return this.icons[codeDiv as any]
	}
	return ''
  }
  
  constructor(private tripService: TripService, private userService: UserService, private cookieService: CookieService, private cd : ChangeDetectorRef) {
	this.userName = null
    if (cookieService.check('id'))
	{
		this.userService.getUserByUserId(Number(cookieService.get('id'))).subscribe(result => {
			this.userName = result.userName
			this.cd.detectChanges()
		})
	}
	this.maxHeight = 92
	if (this.cookieService.check('trip')) {
		this.tripService.getTripById(Number(this.cookieService.get('trip'))).subscribe(result => {
			if (result)
			{
				tripService.getWeather(result.latitude, result.longitude).subscribe(result => {
					this.weather = result
					this.cd.detectChanges()
				})
			}
		})
	}
  }
  
  public isDesktop = window.innerWidth > window.innerHeight
  onResize(event: any)
  {
	this.isDesktop = window.innerWidth > window.innerHeight
	const header = document.getElementById('header')
	if (header)
	{
		this.maxHeight = 100 - header.offsetHeight / window.innerHeight * 100
        this.cd.detectChanges()
	}
  }
  
  isOnTrip()
  {
	//console.log(window.location.href)
	//console.log(this.weather)
	return window.location.href.endsWith('/trip-view')
  }
  
  format_time(s: any) {
	return new Date(s).toLocaleTimeString("ru-RU").substr(0, 5)
  }
}
